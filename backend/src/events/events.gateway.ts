import {
    ConnectedSocket,
    MessageBody,
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer,
    WsResponse,
} from '@nestjs/websockets';
import {from, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Logger} from "@nestjs/common";
import {GameReference, GamesService} from "../games/games.service";
import {Server, Socket} from 'socket.io';
import {GameSessionId, GameSessionStatus} from "../domain/game";
import {UserService} from "../users/user.service";

type GamePlayer = {
    sessionId: GameSessionId,
    username: string
}

@WebSocketGateway({
    cors: {
        origin: '*',
    },
    allowEIO3: true
})
export class EventsGateway {

    private readonly logger = new Logger(EventsGateway.name);

    @WebSocketServer() server: Server;

    constructor(private gamesService: GamesService, private userService: UserService) {
        this.logger.log('Initialized')
    }

    @SubscribeMessage('events')
    findAll(@MessageBody() data: any): Observable<WsResponse<number>> {
        return from([1, 2, 3]).pipe(map(item => ({event: 'events', data: item})));
    }

    @SubscribeMessage('identity')
    async identity(@MessageBody() data: number): Promise<number> {
        this.logger.log('identity ' + data)
        return data;
    }

    @SubscribeMessage('lounge')
    async lounge(): Promise<WsResponse<GameReference[]>> {
        const games = await this.gamesService.getWaitingLounge()
        return {event: 'lounge', data: games};
    }

    @SubscribeMessage('sessions')
    async userSessions(@MessageBody() playerName: string): Promise<WsResponse<GameReference[]>> {
        const games = await this.gamesService.getPlayerSessions(playerName)
        return {event: 'sessions', data: games};
    }

    @SubscribeMessage('joinSession')
    async joinSession(@ConnectedSocket() client: Socket, @MessageBody() gamePlayer: GamePlayer) {
        const room = gamePlayer.sessionId;
        const user = await this.userService.findByUsername(gamePlayer.username)
        const session = await this.gamesService.joinSession(room, user)
        client.emit('joinedSession', room)
        await client.join(room)
        this.server.to(room).emit('gameSession', session)
        if (session.status === GameSessionStatus.Started) {
            // if game has been started send its state
            const state = await this.gamesService.getLatestState(session.id)
            this.server.to(room).emit('gameState', state)
        }
    }

    @SubscribeMessage('leaveSession')
    leaveSession(@ConnectedSocket() client: Socket, @MessageBody() gamePlayer: GamePlayer) {
        const room = gamePlayer.sessionId;
        client.leave(room)
        client.emit('leftSession', room)
    }

}
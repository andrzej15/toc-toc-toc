import {Body, Controller, forwardRef, Get, Inject, Post, Req, UseGuards} from '@nestjs/common';
import {BasicUser, GamesService} from "../games/games.service";
import {CreateSessionDto} from "./session.dto";
import {JwtAuthGuard} from "../auth/jwt-auth.guard";
import {EventsGateway} from "../events/events.gateway";

export type SessionId = {
    sessionId: string
}

@Controller('session')
export class SessionController {
    constructor(private readonly gamesService: GamesService) {
    }

    @UseGuards(JwtAuthGuard)
    @Post()
    async create(@Req() req: any, @Body() createSessionDto: CreateSessionDto): Promise<SessionId> {
        const user = {
            id: req.user.userId,
            username: req.user.username
        } as BasicUser
        const gameSession = await this.gamesService.createSession(createSessionDto.gameId,
            user, createSessionDto.opponentType)
        return {sessionId: gameSession.id}
    }
}

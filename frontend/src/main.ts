import {createApp} from "vue";
import {createPinia} from "pinia";
import BootstrapVue3, {BToastPlugin} from 'bootstrap-vue-3'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue-3/dist/bootstrap-vue-3.css'
import App from "./App.vue";
import router from "./router";
import VueSocketIO from 'vue-3-socket.io'

const app = createApp(App);

const baseUrl = import.meta.env.VITE_APP_BASE_URL
console.log(`base url: ${baseUrl}`)

app.use(createPinia());
app.use(router);
app.use(BootstrapVue3)
app.use(BToastPlugin)
app.use(new VueSocketIO({
    debug: true,
    connection: baseUrl
}))
app.mount("#app");

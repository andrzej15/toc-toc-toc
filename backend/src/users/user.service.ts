import {Injectable, Logger} from '@nestjs/common';
import {InjectRedis} from "@liaoliaots/nestjs-redis";
import Redis from 'ioredis';
import {randomUUID} from "crypto";
import {DataAccessError} from "../dao";

export class UserAlreadyExists extends Error {
    code: string

    constructor(message: string) {
        super(message);
        this.code = "USER_ALREADY_EXISTS";
    }
}

// This should be a real class/interface representing a user entity
export type User = {
    id: string,
    username: string,
    password: string
};

@Injectable()
export class UserService {

    private readonly logger = new Logger(UserService.name);

    constructor(
        @InjectRedis('user') private readonly redis: Redis
    ) {
        this.saveUser({id: randomUUID(), username: "a", password: "a"})
            .catch(e => this.logger.warn(e))
        this.saveUser({id: randomUUID(), username: "b", password: "b"})
            .catch(e => this.logger.warn(e))
    }

    async findByUsername(username: string): Promise<User | null> {
        const id = await this.redis.get('username:' + username)
        if (id === null) {
            return null
        }
        return this.findById(id)
    }

    async findById(id: string): Promise<User | null> {
        return this.redis.hgetall('id:' + id)
            .then(user => user as User)
    }

    async createUser(username: string, password: string) {
        const user = {username, password, id: randomUUID()} as User
        await this.saveUser(user)
    }

    async saveUser(user: User) {
        const existingUser = await this.findByUsername(user.username)
        if (existingUser !== null) {
            throw new UserAlreadyExists(`User [${user.username}] already exists`)
        }
        return this.redis
            .multi()
            .hset('id:' + user.id, user)
            .set('username:' + user.username, user.id)
            .exec((err, result) => {
                if (err) {
                    throw new DataAccessError(err.message)
                }
                return result
            })
    }
}

export const BASE_URL = "/api"
export const HTTP_JSON_HEADERS = {
    headers: {
        'Content-Type': 'application/json'
    }
}
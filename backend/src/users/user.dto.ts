import {IsNotEmpty, Validate} from "class-validator";
import {NotEmailRule} from "./user.validator";

export class UserDto {
    @IsNotEmpty()
    @Validate(NotEmailRule)
    username: string;

    @IsNotEmpty()
    password: string;
}
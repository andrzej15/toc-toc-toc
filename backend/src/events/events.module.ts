import { Module } from '@nestjs/common';
import { EventsGateway } from './events.gateway';
import {GamesModule} from "../games/games.module";
import {UserModule} from "../users/user.module";

@Module({
    imports: [GamesModule, UserModule],
    providers: [EventsGateway],
})
export class EventsModule {}
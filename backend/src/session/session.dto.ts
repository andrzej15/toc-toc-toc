import {OpponentType} from "../domain/game";

export class CreateSessionDto {
    gameId: string
    opponentType: OpponentType
}
import {Injectable} from "@nestjs/common";
import {
    Game,
    GameId,
    GameService,
    GameSession,
    GameSessionId,
    GameSessionStatus,
    GameState,
    Player,
} from "../domain/game";
import {InjectRedis} from "@liaoliaots/nestjs-redis";
import Redis from "ioredis";
import {DataAccessError} from "../dao";
import {GameRepository} from "../game/game.service";

export type GameSessionDto = {
    id: GameSessionId
    gameId: GameId
    status: GameSessionStatus
    players: string // serialized players
    createdAt: string
}

const toSessionDto = (gameSession: GameSession<any>): GameSessionDto => ({
    id: gameSession.id,
    gameId: gameSession.game.id,
    status: gameSession.status,
    players: JSON.stringify(gameSession.players),
    createdAt: gameSession.createdAt.toUTCString()
})

const toGameSession = (gameService: GameService, dto: GameSessionDto): GameSession<any> => {
    const players: Player[] = JSON.parse(dto.players)
    return new GameSession(
        dto.id,
        gameService.getGameById(dto.gameId),
        dto.status,
        players,
        new Date(dto.createdAt)
    )
}

@Injectable()
export class SessionService {

    constructor(
        @InjectRedis('session') private readonly redis: Redis,
        private gameRepository: GameRepository
    ) {
    }

    async saveState(state: GameState<Game>) {
        await this.redis.lpush('game-state:' + state.gameId, JSON.stringify(state))
    }

    async getLatestState(sessionId: GameSessionId): Promise<GameState<Game>> {
        const jsonState = await this.redis.lrange('game-state:' + sessionId, 0, 0)
            .then(values => values.length > 0 ? values[0] : null)
        return Object.assign(GameState.prototype, JSON.parse(jsonState)) as GameState<Game>
    }

    async saveSession(gameSession: GameSession<any>) {
        const gameSessionDto = toSessionDto(gameSession)
        const sessionId = gameSessionDto.id;

        // save game session
        this.redis.hset('session:' + sessionId, gameSessionDto)
        if (gameSession.canBeStarted()) {
            // all players joined - append game to ongoing games
            await this.redis.sadd('ongoing', sessionId)
            await this.redis.srem('waiting-lounge', sessionId)
        } else {
            // waiting for more players
            await this.redis.sadd('waiting-lounge', sessionId)
        }

        const humanPlayers = gameSession.humanPlayers()
        for (const player of humanPlayers) {
            // append to lists indexed by player's name
            const playerName = player.name
            await this.redis.sadd('user-session:' + playerName, sessionId)
            if (gameSession.canBeStarted()) {
                await this.redis.sadd('user-ongoing:' + playerName, sessionId)
                await this.redis.srem('user-waiting-lounge:' + playerName, sessionId)
            } else {
                await this.redis.sadd('user-waiting-lounge:' + playerName, sessionId)
            }
        }
    }

    async getSession(sessionId: GameSessionId): Promise<GameSession<Game> | null> {
        const session = await this.redis.hgetall('session:' + sessionId)
        return toGameSession(this.gameRepository, session as unknown as GameSessionDto)
    }

    async getPlayerSessions(playerName: string): Promise<GameSession<Game>[]> {
        return this.getGameSessionsByKey('user-session:' + playerName)
    }

    async getWaitingLounge(): Promise<GameSession<Game>[]> {
        return this.getGameSessionsByKey('waiting-lounge')
    }

    private async getGameSessionsByKey(key: string): Promise<GameSession<Game>[]> {
        const sessionIds = await this.redis.smembers(key)
        const pipeline = this.redis.pipeline()
        for (const sessionId of sessionIds) {
            pipeline.hgetall('session:' + sessionId)
        }
        return pipeline
            .exec((err, results) => {
                if (err) {
                    throw new DataAccessError(err.message)
                }
                return results
            })
            .then((results) => {
                const gameSessions = []
                for (const result of results) {
                    const resultError = result[0]
                    if (resultError !== null) {
                        throw new DataAccessError(resultError.message)
                    }
                    const sessionDto = result[1] as GameSessionDto
                    gameSessions.push(toGameSession(this.gameRepository, sessionDto))
                }
                return gameSessions
            })
    }

}
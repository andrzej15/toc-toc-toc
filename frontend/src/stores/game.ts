import {defineStore} from "pinia";
import {useAuthStore} from "@/stores/auth";
import {AuthenticatedGameService} from "@/services/GameService";

export const useGameStore = defineStore('game', {
    getters: {
        gameService() {
            const authentication = useAuthStore()
            const token = authentication.token
            if (token !== null) {
                return new AuthenticatedGameService(token)
            }
        }
    }
});

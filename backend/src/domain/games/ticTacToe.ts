import {Gameplay, Game, GameError, GameId, GameMove, GameState, GameStatus, Player, GameSession} from "../game";
import Board from "tictactoe-board";
import {randomUUID} from "crypto";

export class TicTacToe implements Game {
    id = "tic-tac-toe"
    name = "Tic-tac-toe"
    numberOfPlayers = 2
    allowComputerPlayers = false
}

export const TIC_TAC_TOE = new TicTacToe()

export class TicTacToeState extends GameState<TicTacToe> {
    readonly board: string[]

    constructor(id: string, gameId: GameId, status: GameStatus, players: Player[], playerToMove: Player,
                board: string[], winner: Player = null) {
        super(id, gameId, status, players, playerToMove, winner);
        this.board = board;
    }
}

export class TicTacToeMove implements GameMove<TicTacToe> {
    readonly index: number // index on the board to
    constructor(index: number) {
        if (index < 1 || index > 9) {
            throw new GameError("Invalid move")
        }
        this.index = index;
    }
}

export class TicTacToeGameplay implements Gameplay<TicTacToe> {
    start(gamePlay: GameSession<TicTacToe>): TicTacToeState {
        const players = gamePlay.players
        if (players?.length != TIC_TAC_TOE.numberOfPlayers) {
            throw new GameError(`Invalid number of players. Expected ${TIC_TAC_TOE.numberOfPlayers}`)
        }
        const board = new Board();
        return new TicTacToeState(randomUUID(), gamePlay.id, GameStatus.Started, players, players[0], board.grid)
    }

    move(state: TicTacToeState, player: Player, move: TicTacToeMove): TicTacToeState {
        const board = new Board(state.board)
        const nextPlayer = state.nextPlayer()
        const status = GameStatus.Started
        const newBoard = board.makeMove(move.index, board.currentMark());
        return new TicTacToeState(randomUUID(), state.gameId, status, state.players, nextPlayer, newBoard.grid)
    }

}
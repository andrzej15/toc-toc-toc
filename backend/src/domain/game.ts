import {randomUUID} from "crypto";

export type GameId = string
export type PlayerId = string
export type GameSessionId = string

export type Game = {
    id: GameId
    name: string
    numberOfPlayers: number
    allowComputerPlayers: boolean
}

export type OpponentType = "human" | "computer"

export type PlayerType = {
    type: OpponentType
}

export type HumanPlayer = PlayerType & {
    type: "human"
    id: PlayerId
    name: string
}

export type ComputerPlayer = PlayerType & {
    type: "computer"
    id: string
}

export const humanPlayer = (id: PlayerId, name: string): HumanPlayer => ({id, name, type: "human"});
export const computerPlayer = (id: string): ComputerPlayer => ({id, type: "computer"});

export type Player = HumanPlayer | ComputerPlayer

export interface GameMove<T extends Game> {
}

export enum GameSessionStatus {
    WaitingForOpponents = 'WAITING',
    Started = 'STARTED',
    Finished = 'FINISHED'
}

export enum GameStatus {
    Started,
    Finished
}

export class GameState<T extends Game> {
    readonly id: string
    readonly gameId: GameId
    readonly status: GameStatus
    readonly players: Player[]
    readonly playerToMove: Player
    readonly winner?: Player
    readonly createdAt: Date

    constructor(id: string, gameId: string, status: GameStatus, players: Player[], playerToMove: Player,
                winner: Player, createdAt?: Date) {
        this.id = id;
        this.gameId = gameId;
        this.status = status;
        this.players = players;
        this.playerToMove = playerToMove;
        this.winner = winner;
        this.createdAt = createdAt ? createdAt : new Date();
    }

    public nextPlayer(): Player {
        const players = this.players;
        const currentPlayerIndex = players.findIndex(p => p == this.playerToMove)
        return players[(currentPlayerIndex + 1) % players.length]
    }

}

export class GameError extends Error {
}

export interface Gameplay<T extends Game> {
    start(gamePlay: GameSession<T>): GameState<T>

    move(state: GameState<T>, player: Player, move: GameMove<T>): GameState<T>
}

export class GameSession<T extends Game> {
    readonly id: GameSessionId
    readonly game: T
    status: GameSessionStatus
    players: Player[]
    readonly createdAt: Date

    constructor(id: GameSessionId, game: T, status: GameSessionStatus, players: Player[], createdAt?: Date) {
        this.id = id;
        this.game = game;
        this.status = status;
        this.players = [];
        this.createdAt = createdAt ? createdAt : new Date();
        for (const player of players) {
            this.joinGame(player)
        }
    }

    playersCanJoin(): boolean {
        return this.game.numberOfPlayers > this.players.length
    }

    canBeStarted(): boolean {
        return this.game.numberOfPlayers == this.players.length
    }

    alreadyJoined(player: Player): boolean {
        return this.players.some(({type, id}) => player.type == type && player.id == id)
    }

    joinGame(player: Player): boolean {
        if (this.alreadyJoined(player)) {
            return false
        }
        if (this.playersCanJoin()) {
            this.players.push(player)
        }
        return true
    }

    startGame(): boolean {
        if (this.canBeStarted()) {
            this.status = GameSessionStatus.Started
            return true
        } else {
            return false
        }
    }

    humanPlayers(): HumanPlayer[] {
        return this.players.filter(player => player.type === 'human') as HumanPlayer[]
    }

    creator(): Player {
        return this.players[0]
    }
}

export interface GameService {
    getAllGames(): Game[]

    getGameById(gameId: GameId): Game | null
}

export class GameSessionService {
    create(game: Game, player: Player, opponentType: OpponentType): GameSession<any> {
        const players = [player]
        if (opponentType == 'computer') {
            // create computer opponents if needed
            for (let i = 1; i < game.numberOfPlayers; i++) {
                players.push(computerPlayer("ai_strategy"))
            }
        }
        return new GameSession(randomUUID(), game, GameSessionStatus.WaitingForOpponents, players)
    }

}

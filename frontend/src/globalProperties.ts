import type {ToastInstance} from "bootstrap-vue-3/dist/components/BToast/plugin";
import type {Socket} from "socket.io-client";

declare module 'vue' {

    interface ComponentCustomProperties {
        toast: ToastInstance
        $socket: Socket
    }
}

export {}
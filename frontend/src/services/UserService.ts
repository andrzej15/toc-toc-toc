import axios from "axios";
import {BASE_URL, HTTP_JSON_HEADERS} from "@/services/config";

export async function signUp(username: string, password: string) {
    const json = JSON.stringify({username, password});
    await axios.post(`${BASE_URL}/users`, json, HTTP_JSON_HEADERS)
}
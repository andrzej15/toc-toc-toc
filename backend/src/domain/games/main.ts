import {Game, Gameplay} from "../game";
import {TIC_TAC_TOE, TicTacToeGameplay} from "./ticTacToe";

export const getAllGames = (): Game[] => [
    TIC_TAC_TOE
];

export function getGameplay<T extends Game>(game: T): Gameplay<T> {
    switch (game.id) {
        case TIC_TAC_TOE.id:
            return new TicTacToeGameplay()
        default:
            throw new Error(`No gameplay for game [${game.id}]`)
    }
}
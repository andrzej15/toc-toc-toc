import {Injectable, Logger} from "@nestjs/common";
import {
    Game,
    GameError,
    GameId,
    GameService, GameSession, GameSessionId,
    GameSessionService, GameState,
    humanPlayer, OpponentType, Player,
} from "../domain/game";
import {UserService} from "../users/user.service";
import {GameSessionDto, SessionService} from "../session/session.service";
import {GameRepository} from "../game/game.service";

export type BasicUser = {
    id: string,
    username: string
}

export type GameReference = {
    id: string
    game: string
    creator: Player
    status: string
}

@Injectable()
export class GamesService {
    private readonly gameSessionService: GameSessionService
    private readonly logger = new Logger(GamesService.name);

    constructor(private userService: UserService,
                private sessionService: SessionService,
                private gameRepository: GameRepository) {
        this.gameSessionService = new GameSessionService()
    }

    getAllGames = (): Game[] => this.gameRepository.getAllGames()

    private getGameById(gameId: GameId): Game {
        const game = this.gameRepository.getGameById(gameId)
        if (!game) {
            throw new GameError("Invalid game id.")
        }
        return game
    }

    async createSession(gameId: GameId, user: BasicUser, opponentType: OpponentType): Promise<GameSession<Game>> {
        const player = humanPlayer(user.id, user.username)
        const game = this.getGameById(gameId)
        const gameSession = this.gameSessionService.create(game, player, opponentType)
        await this.sessionService.saveSession(gameSession)
        return gameSession
    }

    async joinSession(sessionId: GameSessionId, user: BasicUser): Promise<GameSession<Game>> {
        const player = humanPlayer(user.id, user.username)
        const session = await this.sessionService.getSession(sessionId)
        const joined = session.joinGame(player)
        if (joined) {
            this.logger.log(`Player joined session ${sessionId}`, session)
            await this.sessionService.saveSession(session)
        }
        if (session.startGame()) {
            this.logger.log(`Starting game for session [${session.id}]`)
            // start actual gamep
            const gameplay = this.gameRepository.getGameplayByGame(session.game)
            const state = gameplay.start(session)
            await this.sessionService.saveState(state)
        }
        return session
    }

    async getLatestState(sessionId: GameSessionId): Promise<GameState<Game>> {
        return this.sessionService.getLatestState(sessionId)
    }

    async getWaitingLounge(): Promise<GameReference[]> {
        const sessions = await this.sessionService.getWaitingLounge()
        return toGameReference(sessions)
    }

    async getPlayerSessions(playerName: string): Promise<GameReference[]> {
        const sessions = await this.sessionService.getPlayerSessions(playerName)
        return toGameReference(sessions)
    }

}

const toGameReference = (sessions: GameSession<Game>[]): GameReference[] => sessions.map(session => ({
    id: session.id,
    game: session.game.name,
    creator: session.creator(),
    status: session.status
}))

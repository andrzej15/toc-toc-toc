import {Module} from '@nestjs/common';
import {GameRepository} from "./game.service";

@Module({
    providers: [GameRepository],
    exports: [GameRepository],
})
export class GameModule {
}
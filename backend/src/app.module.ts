import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AuthModule} from './auth/auth.module';
import {UserModule} from './users/user.module';
import {GamesController} from './games/games.controller';
import {GamesModule} from './games/games.module';
import {SessionController} from './session/session.controller';
import {ConfigModule, ConfigService} from '@nestjs/config';
import {RedisModule, RedisModuleOptions} from "@liaoliaots/nestjs-redis";
import {UserController} from "./users/user.controller";
import {EventsModule} from "./events/events.module";
import {GameModule} from "./game/game.module";

const ENV = process.env.NODE_ENV;

@Module({
    imports: [
        ConfigModule.forRoot({
            envFilePath: !ENV ? '.env' : `.env.${ENV}`,
        }),
        AuthModule, UserModule, GameModule, GamesModule, EventsModule,
        RedisModule.forRootAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: async (configService: ConfigService): Promise<RedisModuleOptions> => {
                return {
                    config: [
                        {
                            url: configService.get("REDIS_URL")
                        },
                        {
                            namespace: 'user',
                            url: configService.get("REDIS_URL")
                        },
                        {
                            namespace: 'session',
                            url: configService.get("REDIS_URL")
                        }
                    ],
                };
            }
        })
        ,],
    controllers: [AppController, GamesController, SessionController, UserController]
})
export class AppModule {
}

import {Injectable} from "@nestjs/common";
import {Game, GameId, Gameplay, GameService,} from "../domain/game";
import {getAllGames, getGameplay} from "../domain/games/main";

@Injectable()
export class GameRepository implements GameService {
    getAllGames(): Game[] {
        return getAllGames();
    }

    getGameById(gameId: GameId): Game | null {
        return this.getAllGames().find(g => g.id === gameId) || null
    }

    getGameplayByGame<T extends Game>(game: T): Gameplay<T> {
        return getGameplay(game)
    }
}

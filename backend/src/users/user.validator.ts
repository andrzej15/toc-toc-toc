import {
    isEmail,
    ValidationArguments,
    ValidatorConstraint,
    ValidatorConstraintInterface
} from "class-validator";
import {Injectable} from "@nestjs/common";

@ValidatorConstraint({name: 'NotEmail', async: false})
@Injectable()
export class NotEmailRule implements ValidatorConstraintInterface {

    validate(value: string) {
        return !isEmail(value);
    }

    defaultMessage(args: ValidationArguments) {
        return 'Email cannot be used as username.';
    }
}
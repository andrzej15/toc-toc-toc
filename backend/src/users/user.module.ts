import {Module} from '@nestjs/common';
import {UserService} from './user.service';
import {UserController} from './user.controller';
import {NotEmailRule} from "./user.validator";

@Module({
    providers: [UserService, NotEmailRule],
    exports: [UserService],
    controllers: [UserController],
})
export class UserModule {
}
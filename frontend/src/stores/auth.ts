import {defineStore} from "pinia";
import AuthService from "@/services/AuthService";

export type AuthState = {
    username: string | null
    token: string | null
}

export const useAuthStore = defineStore('auth', {
    state: () => ({
        username: null,
        token: null
    } as AuthState),
    getters: {
        authenticated: (state) => state.username !== null

    },
    actions: {
        async authenticate(username: string, password: string): Promise<string | null> {
            try {
                const token = await AuthService.authenticate(username, password)
                this.username = username
                this.token = token
                return null
            } catch (e: any) {
                return e.message
            }
        },
        logout() {
            this.username = null;
            this.token = null
        }
    },
});

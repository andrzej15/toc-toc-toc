export {};

declare global {
    namespace NodeJS {
        interface ProcessEnv {
            VUE_APP_BASE_URL: string;
        }
    }
}
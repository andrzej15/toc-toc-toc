import {Game, GameState} from "./game";

export  interface GameStateSerializer {
    serialize(state: GameState<any>): string

    deserialize<T extends Game>(serializedState: string): GameState<Game>
}

export class JsonSerializer implements GameStateSerializer {
    serialize(state: GameState<any>): string {
        return JSON.stringify(state)
    }

    deserialize<T extends Game>(serializedState: string): GameState<Game> {
        return Object.assign({} as T, JSON.parse(serializedState))
    }
}
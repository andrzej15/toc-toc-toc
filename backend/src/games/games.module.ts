import {Module} from '@nestjs/common';
import {GamesService} from "./games.service";
import {UserModule} from "../users/user.module";
import {SessionModule} from "../session/session.module";
import {GameModule} from "../game/game.module";

@Module({
    imports: [UserModule, SessionModule, GameModule],
    providers: [GamesService],
    exports: [GamesService],
})
export class GamesModule {}
# poc-poc-poc

## Executive summary

The base product spec is not implemented but the application is in state, that the rest of the features should be purely labour-intensive. 

What's done:

- build system using docker compose
- ability to login and sign up (using OAuth tokens)
- waiting lounge and "my games"
- ability to start a game (human vs human)
- game screen (game state, without moves and proper game rendering)

## Context

I've spent 4 evenings developing this apps from scratch. I've only worked with Vue before (v2 but I've decided to try v3 here). Decided to go with NestJS as this is the framework you're using. Redis and Socket.IO were new to me too (although I was aware of their features - just lacking hands-on experience). Last time I've used node on backend about a 1.5 years ago.

## Architectural Decisions

1. Every component must support TypeScript. Strong typing is a must to develop almost any non-trivial node application.
2. Use websockets for real-time communication. It's more scalable than using clients with polling.
3. Use Redis as primary database. It's fast, scalable, supports TTL and pub/sub.
4. Domain logic (games and gameplay) is separated from the implementation. This allows easy implementation of new games. 

## Application 

### Frontend

#### Technologies

- https://vuejs.org/ - Nice and easy js framework. Great fit for SPA application.
- https://getbootstrap.com/ - Frontend toolkit providing coherent look and feel.
- https://socket.io/ - Popular websocket library. With plenty client and server integrations.
- https://github.com/axios/axios - Popular HTTP client

### Backend

#### Technologies

- https://nestjs.com/ - All-in-one framework with many integrations. Eliminates ton of boilerplate when creating extensible application.
- https://redis.io/ - Database with pub/sub support. Provides HA, eventually.

## Components

### Games 

Domain logic is defined in the backend - `domain` package:

- `Game` - game definition
- `GameSession` - represents ongoing game 
- `GameState` - serializable "snapshot" of the game

Games are implemented in `domain/games` and exposed via two factories: `getAllGames` and `getGameplay(Game)`

#### Tic tac toe

- `TicTacToe` - defines board ("flattened" matrix with `X` and `O`)
- `TicTacToeMove` - defines game moves (index points position when places his "mark")
- `TicTacToeGameplay` - game logic (missing winner computation logic)

### Authentication and access control

Authentication implemented using JWT tokens (`src/auth` module). Password encryption is missing. Endpoints can be protected (using `@UseGuards(JwtAuthGuard)`). Websocket part needs authentication (e.g. sending and validating token on new client connection).

### Asynchronous events

Real-time features are in `src/events` module. Some of the endpoints should be move reactive (e.g. broadcast `lounge` event when new game is created). 

### Application logic

The most of the application logic is glued by `src/games` module. `GamesService` glues game logic (gameplay) with state persistence (`SessionService`).

## Building and Running

Build and run applications in docker compose:
```sh
./start-app.sh
```
Then navigate to http://localhost:8080/

There are two pre-created users available: `a` (with pass: `a`) and `b` (with pass: `b`)

### Usage scenario

1. Create (Sign up) new user
2. Login with the new user
3. Create a game - after refreshing it should be visible in Lounge and My Games
4. Create another user and join the previously created game
5. Upon joinig a page with game state is presented (including a tic-tac-toe board state - not properly rendered though)

## Next Steps (milestones)

1. Complete tic-tac-toe logic
   1. Server - implement end-game logic (win, lose, draw)
   2. Server - add 'make move' logic
   3. Client - render board and allow making moves
2. Implement proper security
   1. Secure websocket communication
   2. Encrypt (and salt) user passwords
   3. Game constraints (e.g. play one game at a time)
3. Refine client-server communication
   1. Auto-refreshing of "boards"
4. More base features
   1. Game counters
   2. Searching boards by username
5. Implement AI - players of type 'computer'
6. Additional features...

## Scalability

In most of the cases horizontal scalability will be just fine:

- Adding more Redis nodes
- Adding more server instances (websocket gateways)
- CDN to distribute frontend (and adjusting the build to load particular game UI on demand)
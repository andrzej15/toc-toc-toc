import {TIC_TAC_TOE, TicTacToe, TicTacToeGameplay, TicTacToeMove} from "./ticTacToe";
import {GameError, GameSession, GameSessionStatus, GameStatus, humanPlayer} from "../game";

test('invalid number of players throws exception', () => {
    const gameLogic = new TicTacToeGameplay()
    const players = [];
    const gamePlay = new GameSession<TicTacToe>("game-id", TIC_TAC_TOE, GameSessionStatus.WaitingForOpponents, players)
    expect(() => {
        gameLogic.start(gamePlay)
    }).toThrow(GameError)
})

test('starting new game', () => {
    const gameLogic = new TicTacToeGameplay()
    const players = [player("player1"), player("player2")];
    const gamePlay = new GameSession<TicTacToe>("game-id", TIC_TAC_TOE, GameSessionStatus.WaitingForOpponents, players)
    const state = gameLogic.start(gamePlay)
    expect(state.players).toEqual(players)
    expect(state.playerToMove).toEqual(player("player1"))
    expect(state.board).toEqual(["", "", "", "", "", "", "", "", ""])
    expect(state.status).toBe(GameStatus.Started)
})


test('making a move', () => {
    const gameLogic = new TicTacToeGameplay()
    const players = [player("player1"), player("player2")];
    const gamePlay = new GameSession<TicTacToe>("game-id", TIC_TAC_TOE, GameSessionStatus.WaitingForOpponents, players)
    const state = gameLogic.move(gameLogic.start(gamePlay), player("player2"), new TicTacToeMove(1))
    expect(state.board).toEqual(["X", "", "", "", "", "", "", "", ""])
})

const player = (name: string) => humanPlayer(name, name)
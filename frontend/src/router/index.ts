import {createRouter, createWebHistory} from "vue-router";
import HomeView from "@/views/HomeView.vue";
import LoginView from "@/views/LoginView.vue";
import LogoutView from "@/views/LogoutView.vue";
import SignUpView from "@/views/SignUpView.vue";
import GameView from "@/views/GameView.vue";

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: "/",
            name: "home",
            component: HomeView,
        },
        {
            path: "/login",
            name: "login",
            component: LoginView,
        },
        {
            path: "/signup",
            name: "signup",
            component: SignUpView,
        },
        {
            path: "/logout",
            name: "logout",
            component: LogoutView,
        },
        {
            path: "/join",
            name: "join-game",
            component: GameView,
            props: true
        },
    ],
});

export default router;

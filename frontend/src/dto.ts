export type GameReference = {
    id: string
    game: string
    creator: HumanPlayer
    status: string
}

export type Player = HumanPlayer | ComputerPlayer

export type HumanPlayer = PlayerType & {
    type: "human"
    id: string
    name: string
}

export type ComputerPlayer = PlayerType & {
    type: "computer"
    id: string
}

export type OpponentType = "human" | "computer"

export type PlayerType = {
    type: OpponentType
}


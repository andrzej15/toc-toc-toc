import axios, {Axios} from "axios";
import {BASE_URL, HTTP_JSON_HEADERS} from "@/services/config";

export type Game = {
    id: string
    name: string
    numberOfPlayers: number
    allowComputerPlayers: boolean
}

export class RestClient {
    protected client: Axios

    constructor(token: string) {
        this.client = axios.create({
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        })
    }
}

export class AuthenticatedGameService extends RestClient {
    async createGameSession(gameId: string, opponentType: string): Promise<string> {
        const json = JSON.stringify({gameId, opponentType});
        const response = await this.client.post(`${BASE_URL}/session`, json)
        return response.data
    }
}

export async function getAllGames(): Promise<Game[]> {
    const response = await axios.get(`${BASE_URL}/games`, HTTP_JSON_HEADERS)
    return response.data
}


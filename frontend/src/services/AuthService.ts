import axios from "axios";
import {BASE_URL, HTTP_JSON_HEADERS} from "@/services/config";

export default {
    async authenticate(username: string, password: string) {
        const json = JSON.stringify({username, password});
        const response = await axios.post(`${BASE_URL}/auth/login`, json, HTTP_JSON_HEADERS)
        return response.data.access_token
    }
}
import {Body, Controller, HttpException, HttpStatus, Post} from '@nestjs/common';
import {UserService} from "./user.service";
import {UserDto} from "./user.dto";

@Controller('users')
export class UserController {
    constructor(private userService: UserService) {
    }

    @Post()
    async create(@Body() user: UserDto) {
        return this.userService.createUser(user.username, user.password)
            .catch(e => {
                if (e.code && e.code === 'USER_ALREADY_EXISTS') {
                    throw new HttpException({
                        error: "Bad Request",
                        statusCode: HttpStatus.BAD_REQUEST,
                        message: ["User already exists"]
                    }, HttpStatus.BAD_REQUEST)
                } else {
                    throw e
                }
            })
    }

}

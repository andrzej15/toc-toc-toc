import {Module} from '@nestjs/common';
import {SessionService} from "./session.service";
import {GameModule} from "../game/game.module";

@Module({
    imports: [GameModule],
    providers: [SessionService],
    exports: [SessionService],
})
export class SessionModule {}